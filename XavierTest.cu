#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cstring>
#include <iostream>
#include <opencv2/cudev/common.hpp>
#include <opencv2/cudev/ptr2d/glob.hpp>

__global__ void array_sum(int *d_a, int *d_b, int *d_c, int size)
{
    int gid = blockDim.x * blockIdx.x + threadIdx.x;

    if (gid < size)
    {
        d_c[gid] = d_a[gid] + d_b[gid];
    }
}

extern "C" void aSum(const cv::cuda::GpuMat& d_a, const cv::cuda::GpuMat& d_b, cv::cuda::GpuMat* d_c, int size)
{
 	int block_size = 128;
 	dim3 block(block_size);
    dim3 grid(size / block.x);
 	array_sum<<<grid, block>>>((int*)d_a.ptr(), (int*)d_b.ptr(), (int*)d_c->ptr(), size);
}

