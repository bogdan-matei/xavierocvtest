﻿#include <iostream>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafilters.hpp>
#include <opencv2/cudev/common.hpp>
#include <opencv2/cudaarithm.hpp>
#include <opencv2/cudawarping.hpp>
#include <opencv2/cudev/ptr2d/glob.hpp>
#include <opencv2/imgproc/types_c.h>
#include "XavierTest.h"

enum allocator_type
{
    DEFAULT,
    CUSTOM
};

CustomAllocator cAllocator;
CustomAllocator *g_Allocator = &cAllocator;

extern "C" void aSum(const cv::cuda::GpuMat &d_a, const cv::cuda::GpuMat &d_b, cv::cuda::GpuMat *d_c, int size);
extern "C" void process(unsigned char* inBuffer, int width, int height, int channels, unsigned char* outBuffer);

bool CustomAllocator::allocate(cv::cuda::GpuMat *mat, int rows, int cols, size_t elemSize)
{
    /*if (rows > 1 && cols > 1)
    {
         CV_CUDEV_SAFE_CALL(cudaMallocPitch(&mat->data, &mat->step, elemSize * cols, rows));
    }
    else*/
    {
        // Single row or single column must be continuous
        //CV_CUDEV_SAFE_CALL(cudaMalloc(&mat->data, elemSize * cols * rows));
        CV_CUDEV_SAFE_CALL(cudaMallocManaged(&mat->data, elemSize * cols * rows));
        mat->step = elemSize * cols;
    }

    mat->refcount = (int *)cv::fastMalloc(sizeof(int));

    return true;
}

void CustomAllocator::free(cv::cuda::GpuMat *mat)
{
    cudaFree(mat->datastart);
    cv::fastFree(mat->refcount);
}

int main(int argc, const char *argv[])
{
    if (argc != 3)
    {
        printf("Usage: %s [mode] [allocator]\n\nValid modes:\n\t0 = short test\n\t1 = long test\n\nValid allocators:\n\t0 = OpenCV\n\t1 = custom\n", argv[0]);
        exit(1);
    }

    int mode = atoi(argv[1]);
    int allocator = atoi(argv[2]);

    if (allocator == allocator_type::CUSTOM)
    {
        cv::cuda::GpuMat::setDefaultAllocator(g_Allocator);
    }

    switch (mode)
    {
    case 0:
    {
        int size = 1 << 20;
        cv::cuda::GpuMat a(1, size, CV_32S);
        cv::cuda::GpuMat b(1, size, CV_32S);
        cv::cuda::GpuMat c(1, size, CV_32S);

        if (allocator == allocator_type::CUSTOM)
        {
            cv::Mat c_a(1, size, CV_32S, a.data);
            c_a.setTo(10);

            cv::Mat c_b(1, size, CV_32S, b.data);
            c_b.setTo(20);
        }
        else
        {
            cv::Mat c_a(1, size, CV_32S);
            c_a.setTo(10);
            a.upload(c_a);

            cv::Mat c_b(1, size, CV_32S);
            c_b.setTo(20);
            b.upload(c_b);
        }

        aSum(a, b, &c, size);

        cudaDeviceSynchronize();
        
        if (allocator == allocator_type::CUSTOM)
        {
            cv::Mat c_c(c.rows, c.cols, c.type(), c.data);
        }
        else
        {            
            cv::Mat c_c;
            c.download(c_c);
        }

        return 0;
    }
    break;

    case 1:
    {
        cv::String filename = "../baboon.jpg";
        cv::Mat imgCpu = cv::imread(filename);

        cv::cuda::GpuMat imgGpu(imgCpu.rows, imgCpu.cols, imgCpu.type());
        if (allocator == allocator_type::CUSTOM)
        {
            memcpy(imgGpu.data, imgCpu.data, imgCpu.total() * imgCpu.elemSize());
        }
        else
        {
            cv::cuda::GpuMat imgGpu(imgCpu);
        }

        cv::cuda::GpuMat out(imgCpu.rows, imgCpu.cols, imgCpu.type());

        for (int i = 0; i < 100; ++i)
        {
            cv::cuda::cvtColor(imgGpu, out, CV_RGB2BGR, 0);
            
            if (allocator == allocator_type::CUSTOM)
            {
                cv::Mat cpuMat(out.rows, out.cols, out.type(), out.data);
            }
            else
            {
                cv::Mat cpuMat;
                out.download(cpuMat);
            }
        }
    }
    break;

    default:
        break;
    }
}
