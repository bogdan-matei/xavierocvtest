﻿#include <cuda_runtime.h>
#include "opencv2/cudafilters.hpp"

class CustomAllocator : public cv::cuda::GpuMat::Allocator
{
public:
    bool allocate(cv::cuda::GpuMat *mat, int rows, int cols, size_t elemSize) override;
    void free(cv::cuda::GpuMat *mat) override;
};
